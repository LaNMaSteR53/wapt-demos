function filter(str) {
    str = str.replace(/[^a-zA-Z0-9]/g, "");
    return str;
}

function validateForm(e) {
    var username = e["username"].value;
    var password = e["password"].value;
    var username_filtered = filter(username);
    var password_filtered = filter(password);
    if (username != username_filtered || password != password_filtered) {
        alert("Only alphanumeric characters allowed.");
        return false;
    }
}

function makeRequest(url) {
    var xmlhttp;
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("message").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function changePassword() {
    var username = filter(document.forms["form2"]["username"].value);
    var password = filter(document.forms["form2"]["password"].value);
    var password_conf = filter(document.forms["form2"]["password_confirm"].value);
    if (password == password_conf) {
        url = "change_password.php?username=" + username + "&password=" + password + "&password_conf=" + password_conf;
        makeRequest(url);
    }
}
