<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript" src="api.js"></script>
</head>
<body>

<form id="form1" action="#" method="POST" onsubmit="return validateForm(this)">
    <input type="radio" name="level" value="easy" checked>Easy<br />
    <input type="radio" name="level" value="medium">Medium<br />
    <input type="radio" name="level" value="hard">Hard<br />
    <input type="radio" name="level" value="secure">Secure<br /><br />
    Username:<br /><input id="username" type="text" name="username" value="" /><br /><br />
    Password:<br /><input type="password" name="password" value="" /><br /><br />
    <input type="submit" name="submit" value="Login" /><br />
</form><br />

<?php
    $usernames = array("tim", "john", "kevin", "ethan", "jason", "erica");
    $level     = $_REQUEST["level"];
    $username  = $_REQUEST["username"];

    if (isset($username) && $level == 'easy') {
        if (in_array($username, $usernames)) {
            echo "Invalid Password!";
        } else {
            echo "Invalid Username!";
        }
        echo "<script>document.getElementsByName('level')[0].checked = true;</script>";
    }

    if (isset($username) && $level == 'medium') {
        echo "Authentication Failed!";
        if (in_array($username, $usernames)) {
            echo "<script>document.getElementById('username').value = '".$username."';</script>";
        }
        echo "<script>document.getElementsByName('level')[1].checked = true;</script>";
    }

    if (isset($username) && $level == 'hard') {
        echo "Authentication Failed!";
        if (in_array($username, $usernames)) {
            echo "\x00";
        }
        echo "<script>document.getElementsByName('level')[2].checked = true;</script>";
    }

    if (isset($username) && $level == 'secure') {
        echo "Authentication Failed!";
        echo "<script>document.getElementsByName('level')[3].checked = true;</script>";
    }

?>

</body>
</html>
