<?php

    $query_str = "//Employee[UserName='PPan' and Password='{INJECT}']";
    $param = "";
    if (isset($_REQUEST["param"], $_REQUEST["query_str"])) {
        $param = $_REQUEST["param"];
        $query_str = $_REQUEST["query_str"];
    }
    $xml_file = "data.xml";

?>
<!DOCTYPE html>
<html>
<head>
<script>
function decode_data() {
    var decoded_data = window.atob('<?php echo base64_encode(htmlspecialchars(file_get_contents($xml_file))); ?>');
    document.getElementById('xml').innerHTML = '<pre>'+decoded_data+'</pre>'
}
</script>
</head>
<body onload="decode_data();">
<span onmouseover="document.getElementById('xml').style.display='inline';" onmouseout="document.getElementById('xml').style.display='none';">View XML</span><br /><br />
<div id="xml" style="display: none; background-color: white; border: 1px solid black; position: absolute; top: 30px; left: 10px; padding: 0 10px;"></div>
<form action="xpath_injection.php" method="POST">
    Search XML: <input type="text" name="param" value="<?php echo $param ?>" size="80" /><br /><br />
    Query String: <input type="text" name="query_str" value="<?php echo $query_str ?>" size="80" /><br /><br />
    <input type="submit" name="submit" value="Search" />
</form><br />
<?php

    function RecursiveXML( SimpleXMLElement $han, $prefix = "") {
        if( count( $han->children() ) < 1 ) {
            return $prefix . "&lt;" . $han->getName() . WarpAttributes($han->attributes()) . "&gt;" . $han . "&lt;/" . $han->getName() . "&gt;<br />";
        }
        $ret = $prefix . "&lt;" . $han->getName() . WarpAttributes($han->attributes()) . "&gt;<br />";
        foreach( $han->children() as $key => $child ) {
            $ret .= RecursiveXML($child, $prefix . "  " );
        }
        $ret .= $prefix . "&lt;/" . $han->getName() . "&gt;<br />";
        return $ret;
    } 

    function WarpAttributes($attributes) {
        $ret = '';
        foreach($attributes as $a => $b) {
            $ret .= ' '.$a.'="'.$b.'"';
        }
        return $ret;
    }

    if (isset($param)) {
        ini_set('display_errors', 0);
        $xml = simplexml_load_file($xml_file);
        $query = str_replace("{INJECT}", $param, $query_str);
        $results = $xml->xpath($query);
        echo 'Executed query: ' . $query . '<br /><br />';
        if ($results) {
            echo 'Results:<br />';
            echo '<pre>';
            foreach ($results as $result) {
                echo RecursiveXML($result);
            }
            echo '</pre>';
        }
    }

?>
</body>
</html> 
