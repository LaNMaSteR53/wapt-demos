<!DOCTYPE html>
<html>
<body>

<?php
    $username      = $_REQUEST["username"];
    $password      = $_REQUEST["password"];
    $password_conf = $_REQUEST["password_conf"];
    if ($password == $password_conf) {
        echo "Hello, " . $username . ". Your password has been changed.";
    } else {
        echo "Passwords must match.";
    }
?>

</body>
</html>
